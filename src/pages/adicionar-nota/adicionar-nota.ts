import { HomePage } from './../home/home';
import { DetalhesNotaPage } from './../detalhes-nota/detalhes-nota';
import { DadosProvider } from './../../providers/dados/dados';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AdicionarNotaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adicionar-nota',
  templateUrl: 'adicionar-nota.html',
})
export class AdicionarNotaPage {
  nota = {
    titulo: "",
    texto: ""
  };

  acao = "add";
  index;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public dados: DadosProvider
    // public detalhe: DetalhesNotaPage
    ) {  
      this.acao = this.navParams.get('acao');
      console.log(this.acao);

      if (this.acao == "edit") {
        this.index = this.navParams.get('index');
        console.log("Indice: no alterar" + this.index);
        
        this.nota = this.dados.getNote(this.index);
      }
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdicionarNotaPage');
  }

  pop() {
    this.navCtrl.pop();
  }

  salvar() {
    console.log(this.nota.titulo);
    console.log(this.nota.texto);

    if (this.acao == "add") {
      this.dados.addNote(this.nota.titulo, this.nota.texto);
      this.navCtrl.pop();  
    } else {
      this.dados.editNote(this.index, this.nota.titulo, this.nota.texto);
      this.navCtrl.setRoot( HomePage );
    }
    
  }

}
