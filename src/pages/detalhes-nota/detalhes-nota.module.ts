import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesNotaPage } from './detalhes-nota';

@NgModule({
  declarations: [
    DetalhesNotaPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesNotaPage),
  ],
})
export class DetalhesNotaPageModule {}
