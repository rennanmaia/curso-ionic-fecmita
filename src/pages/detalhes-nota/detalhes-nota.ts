import { HomePage } from './../home/home';
import { DadosProvider } from './../../providers/dados/dados';
import { AdicionarNotaPage } from './../adicionar-nota/adicionar-nota';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetalhesNotaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhes-nota',
  templateUrl: 'detalhes-nota.html',
})
export class DetalhesNotaPage {

  nota = {
    titulo: "Nota XYZ",
    texto: "Aqui vai o texto da nota, etc, etc, etc."
  }
  index;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public dados: DadosProvider
    ) {
    this.nota = navParams.get('n');
    this.index = navParams.get('index');
    console.log(this.index);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalhesNotaPage');
  }

  alterar() {
    this.navCtrl.push(AdicionarNotaPage, { acao: 'edit', titulo: this.nota.titulo, texto: this.nota.texto, index: this.index } );
  }

  excluir() {
    this.dados.delNote(this.index);
    this.navCtrl.setRoot( HomePage );
  }

}
