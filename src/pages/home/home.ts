import { AdicionarNotaPage } from './../adicionar-nota/adicionar-nota';
import { DadosProvider } from './../../providers/dados/dados';
import { DetalhesNotaPage } from './../detalhes-nota/detalhes-nota';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // notas = [
  //   {
  //     titulo: "Nota 1",
  //     texto: "Comprar passagem de ônibus"
  //   },
  //   {
  //     titulo: "Nota 2",
  //     texto: "Preparar minicurso Ionic"
  //   },
  //   {
  //     titulo: "Nota 3",
  //     texto: "Assistir trabalhos IC"
  //   },
  // ];
  notas = [];

  constructor(
    public navCtrl: NavController,
    public dados: DadosProvider
    ) {
      this.notas = this.get();
  }

  pushPage(i){
    let n = {
      titulo: this.notas[i].titulo,
      texto: this.notas[i].texto
    }
    this.navCtrl.push(DetalhesNotaPage, { n, index: i });
  }

  get(){
    console.log("Get Notes Provider ");           
    return this.dados.getNotes();
  }

  openPage() {
    this.navCtrl.push(AdicionarNotaPage, { acao: 'add' });
  }



}
