// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the DadosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DadosProvider {

  // notes = [];

  notes = [
    {
      titulo: "Nota 1",
      texto: "Comprar passagem de ônibus"
    },
    {
      titulo: "Nota 2",
      texto: "Preparar minicurso Ionic"
    },
    {
      titulo: "Nota 3",
      texto: "Assistir trabalhos IC"
    },
  ];



  constructor(
    // public http: HttpClient
    ) {
    console.log('Hello DadosProvider Provider');
  }

  getNotes() {
    console.log("Get Notes");
    return this.notes;
  }

  getNote( index ) {
    // let res = this.notes.find(res => res.titulo == obj.titulo, res => res.texto == obj.texto);
    // return res;
    return this.notes[index];
  }

  addNote(titulo: string, texto: string) {
    console.log("Add Note");
    this.notes.push({titulo: titulo, texto: texto});
  }

  editNote(index, titulo, texto) {
    console.log("Edit Note");
    this.notes[index].titulo = titulo;
    this.notes[index].texto = texto;
  }

  delNote(index) {
    console.log("Delete Note");
    this.notes.splice(index,1);
  }

}
